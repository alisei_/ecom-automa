from setuptools import find_packages, setup

setup(
    name='ecom-automa',
    version='1.0',
    author='senseisrl',
    packages=find_packages(include=['src', 'src.*']),
    install_requires=[
        'PyInquirer',
        'pyfiglet',
        'docker',
        'tabulate',
    ],
    entry_points={
        'console_scripts': ['ecom-automa=src.main:main']
    }
)