import docker
from .resize import set_redundancy

def startup(client, host_context):
  startup_redundancy = 1

  for microservice in host_context:    
    try:
      if not(len(microservice.containers)):
        set_redundancy(client, host_context, microservice.name, startup_redundancy)
        print(microservice.name + ": Started")
      else:
        print(microservice.name + ": Already running")
    except docker.errors.APIError:
      print(microservice.name + ": No image found!")
