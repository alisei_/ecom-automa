from PyInquirer import prompt

def resize(client, host_context, avalaible_services):
  service = ask_service(avalaible_services)
  size = ask_size()
  set_redundancy(client, host_context, service, size)

def set_redundancy(client, host_context, service, size):

  for ms in host_context:
    if(ms.name == service):
      microservice = ms
      break

  containers_names = list(map(lambda container: container.name, client.containers.list(filters={"status": "running"})))
  already_running = 0

  for container_name in containers_names:
    if(container_name.count(service) > 0):
      already_running = already_running + 1
  
  nodes = size - already_running
  
  microservice.containers.sort(key=lambda container: container.name)
  
  if len(microservice.containers) > 0:
    last_container_index = microservice.containers[-1].name[-1]
  else:
    last_container_index = -1
  
  if(nodes > 0):
    for i in range(nodes):
      name = "sensei_" + microservice.name + "_" + str(i + int(last_container_index) + 1)
      new_container = client.containers.run(microservice.latest_image, detach=True, name=name)
      microservice.containers.append(new_container)
    print("{} nodes added".format(nodes))
  
  if(nodes < 0):
    for i in range(abs(nodes)):
      microservice.containers[-1].stop()
      microservice.containers[-1].remove()
      microservice.containers.pop()
    print("{} nodes removed".format(abs(nodes)))
  
  if(nodes == 0):
    print("Selected service already have this size")

def ask_service(avalaible_services):
  print("")
  actions_prompt = {
      'type': 'list',
      'name': 'service',
      'message': 'Which service would you like to resize?',
      'choices': avalaible_services
  }

  try:
    answer = prompt(actions_prompt)
    return answer['service'].lower()
  except:
    return False

def ask_size():
  print()
  actions_prompt = {
      'type': 'list',
      'name': 'size',
      'message': 'How many nodes do you want of the selected service?',
      'choices': ["1", "2", "3", "4", "5"]
  }

  try:
    answer = prompt(actions_prompt)
    return int(answer['size'])
  except:
    return False