from tabulate import tabulate

def status(client, host_context):
  table = []
  headers = ["Name", "Is Online", "Redundancy", "Is Latest"]
  for service in host_context:
    row = []
    row.append(service.name)
    row.append(service.is_up)
    row.append(service.size)
    row.append(service.is_fully_updated)

    table.append(row)

  print(tabulate(table, headers=headers))
  
    
