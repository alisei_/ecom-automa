class Microservice:
  def __init__(self, name, db, size, latest_image, containers, is_up):
    self.name = name
    self.db = db
    self.size = size
    self.latest_image = latest_image
    self.containers = containers
    self.is_up = is_up
    self.is_fully_updated = False

  def __repr__(self) -> str:
    return "<'name': {}, 'size': {}>".format(self.name, self.size)

  def to_dict(self):
    properties_dict = []
    properties_dict.insert("name", self.name)
    properties_dict.insert("size", self.size)
    properties_dict.insert("latest_image", self.latest_image)
    properties_dict.insert("containers", self.containers)
    properties_dict.insert("is_up", self.is_up)

    return properties_dict
