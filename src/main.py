""" Ecom-Automa """
import docker
from PyInquirer import prompt
from pyfiglet import Figlet
from datetime import datetime

from .status import status
from .startup import startup
from .resize import resize
from .shutdown import shutdown
from .upgrade import upgrade

from .models.Microservice import Microservice


avalaible_actions = ['Status', 'Resize', 'Upgrade', 'Startup', 'Shutdown', 'Exit']
avalaible_services: list[str] = ['ecom-fe', 'catalog-be', 'cart-be', 'auth', 'balancer']

docker_url = "tcp://192.168.88.97:2375"


def main():
  running = True

  f = Figlet(font='slant')
  print(f.renderText('ecom-automa'))
  print('Welcome to ecom-automa! A small and sweet CLI tool to manage your e-commerce microservices! \n')

  global client
  
  while (running != False):    
    client = docker.DockerClient(docker_url)
    host_context = get_context()

    running = ask_action(client, host_context)


def ask_action(client, host_context):
  print("")
  actions_prompt = {
      'type': 'list',
      'name': 'action',
      'message': 'Which action would you like to perform?',
      'choices': avalaible_actions
  }

  try:
    answer = prompt(actions_prompt)
    action = answer['action'].lower()
  except:
    return False

  if(action == "status"):
    status(client, host_context)
  if(action == "startup"):
    startup(client, host_context)
  if(action == "resize"):
    resize(client, host_context, avalaible_services)
  if(action == "shutdown"):
    shutdown(client, host_context)
  if(action == "upgrade"):
    upgrade(client, host_context, avalaible_services)
  if(action == "exit"):
    return False


def get_context():
  microservices = []

  for service in avalaible_services:
    name = service

    db = ""
    if (name == 'cart-be'):
      db = 'mongo'
    if (name == 'catalog-be'):
      db = 'sql'

    image = "sensei/" + name + ":latest"

    host_images = client.images.list(image)

    latest_image = None

    service_images = list(filter(lambda image: str(image.attrs["RepoTags"]).count(name) > 0, host_images))

    if(len(service_images) > 0):
      latest_image = max(service_images, key=lambda k: datetime.strptime(
          k.attrs["Created"].split('.', 1)[0].replace("T", " "), '%Y-%m-%d %H:%M:%S'))

    containers = client.containers.list(filters={"ancestor": image})

    size = len(containers)
    is_up = False
    for container in containers:
      if(container.status == "running"):
        is_up = True
        break

    ms = Microservice(name, db, size, latest_image, containers, is_up)
    microservices.append(ms)

    for microservice in microservices:
      if(microservice.latest_image != None):
        microservice.is_fully_updated = False

        containers_ts = list(map(lambda c: c.image.attrs["Created"], containers))

        if(containers_ts.count(microservice.latest_image.attrs["Created"]) == len(containers_ts)):
          microservice.is_fully_updated = True

      if(microservice.db and not list(map(lambda container: container.name, client.containers.list())).count(microservice.db) > 0):
        microservice.is_up = False

  return microservices
